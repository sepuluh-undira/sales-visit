<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $fillable = [
        'id',
        'name',
        'stok'
    ];

    const UPDATED_AT = null;
    protected $table = 'barangs';
}
