<?php

namespace App\Http\Controllers;

use App\Barang;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = Barang::get();
        return view('barang.index', compact('list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('barang.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'name.required' => 'Mohon isi nama barang terlebih dahulu',
            'name.alpha_num' => 'Pastikan value yang diinput adalah alfabet dan numeric',
            'stok.numeric' => 'Pastikan value yang diinput adalah numeric'
        ];
        $validator = Validator::make($request->all(), [
            'name' => 'required|alpha_num',
            'stok' => 'numeric'
        ], $messages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::back()->withErrors($messages)->withInput($request->all());
        }
        $barang = new Barang();
        $barang->name = $request->input('name');
        $barang->stok = $request->input('stok') ?? 0;
        $barang->created_by = Auth::user()->email;
        $barang->save();

        return \redirect('barang')->with('success', 'Tambah data berhasil');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $detail = Barang::find($id);

        return view('barang.detail', compact('detail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $detail = Barang::find($id);

        return view('barang.edit', compact('detail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'name.required' => 'Mohon isi nama barang terlebih dahulu',
            'name.alpha_num' => 'Pastikan value yang diinput adalah alfabet dan numeric',
            'stok.numeric' => 'Pastikan value yang diinput adalah numeric'
        ];
        $validator = Validator::make($request->all(), [
            'name' => 'required|alpha_num',
            'stok' => 'numeric'
        ], $messages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::back()->withErrors($messages)->withInput($request->all());
        }
        $barang = Barang::find($id);
        $barang->name = $request->input('name');
        $barang->stok = $request->input('stok') ?? 0;
        $barang->save();

        return \redirect('barang')->with('success', 'Ubah data berhasil');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $barang = Barang::find($id);
        $barang->delete();

        return \redirect('barang')->with('success', 'Delete data berhasil');
    }
}
