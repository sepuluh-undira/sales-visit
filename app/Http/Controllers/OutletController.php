<?php

namespace App\Http\Controllers;

use App\Outlet;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class OutletController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = Outlet::get();
        return view('outlet.index', compact('list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('outlet.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'name.required' => 'Mohon isi nama outlet terlebih dahulu',
            'name.alpha_num' => 'Pastikan value yang diinput adalah alfabet dan numeric',
        ];
        $validator = Validator::make($request->all(), [
            'name' => 'required|alpha_num',
        ], $messages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::back()->withErrors($messages)->withInput($request->all());
        }
        $outlet = new Outlet();
        $outlet->name = $request->input('name');
        $outlet->created_by = Auth::user()->email;
        $outlet->save();

        return \redirect('outlet')->with('success', 'Ubah data berhasil');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $detail = Outlet::find($id);

        return view('outlet.detail', compact('detail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $detail = Outlet::find($id);

        return view('outlet.edit', compact('detail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'name.required' => 'Mohon isi nama outlet terlebih dahulu',
            'name.alpha_num' => 'Pastikan value yang diinput adalah alfabet dan numeric',
        ];
        $validator = Validator::make($request->all(), [
            'name' => 'required|alpha_num',
        ], $messages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::back()->withErrors($messages)->withInput($request->all());
        }
        $outlet = Outlet::find($id);
        $outlet->name = $request->input('name');
        $outlet->save();

        return \redirect('outlet')->with('success', 'Ubah data berhasil');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $outlet = Outlet::find($id);
        $outlet->delete();

        return \redirect('outlet')->with('success', 'Delete data berhasil');
    }
}
