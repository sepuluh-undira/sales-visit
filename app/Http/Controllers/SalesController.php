<?php

namespace App\Http\Controllers;

use App\Sales;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = Sales::get();
        return view('sales.index', compact('list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sales.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'name.required' => 'Mohon isi nama sales terlebih dahulu',
            'email.required' => 'Mohon isi email terlebih dahulu',
            'password.required' => 'Mohon isi password terlebih dahulu',
            'password.min'=> 'Mohon isi password minimal 8 karakter',
            'email.required'=> 'Mohon isi email terlebih dahulu',
            'email.email'=> 'Pastikan format email benar',
            'email.unique'=> 'Email sudah terdaftar'
        ];
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'password' => 'required|min:8',
            'email' => 'required|email|unique:users'
        ], $messages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::back()->withErrors($messages)->withInput($request->all());
        }
        $sales = new Sales();
        $sales->name = $request->input('name');
        $sales->email = $request->input('email');
        $sales->password = Hash::make($request->input('password'));
        $sales->created_by = Auth::user()->email;
        $sales->save();
        User::create([
            'name' => $sales->name,
            'email' => $sales->email,
            'password' => $sales->password,
            'level' => 2
        ]);

        return \redirect('sales')->with('success', 'Ubah data berhasil');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $detail = Sales::find($id);

        return view('sales.detail', compact('detail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $detail = Sales::find($id);

        return view('sales.edit', compact('detail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'name.required' => 'Mohon isi nama sales terlebih dahulu',
            'name.alpha_num' => 'Pastikan value yang diinput adalah alfabet dan numeric',
            'email.required' => 'Mohon isi email terlebih dahulu',
            'password.required' => 'Mohon isi password terlebih dahulu',
            'password.min'=> 'Mohon isi password minimal 8 karakter',
            'email.required'=> 'Mohon isi email terlebih dahulu',
            'email.email'=> 'Pastikan format email benar'
        ];
        $validator = Validator::make($request->all(), [
            'name' => 'required|alpha_num',
            'password' => 'required|min:8',
            'email' => 'required|email'
        ], $messages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::back()->withErrors($messages)->withInput($request->all());
        }
        $sales = Sales::find($id);

        $prev_email = $sales->email;

        $sales->name = $request->input('name');
        $sales->email = $request->input('email');
        $sales->password = Hash::make($request->input('password'));
        $sales->save();
        
        $user = User::where('email',$prev_email)->first();
        $user->name = $sales->name;
        $user->email = $sales->email;
        $user->password = $sales->password;
        $user->save();

        return \redirect('sales')->with('success', 'Ubah data berhasil');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sales = Sales::find($id);
        $sales->delete();

        return \redirect('sales')->with('success', 'Delete data berhasil');
    }
}
