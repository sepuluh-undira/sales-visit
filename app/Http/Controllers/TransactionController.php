<?php

namespace App\Http\Controllers;

use App\Transaction;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userSalesId = DB::table('sales')->where('email', Auth::user()->email)->value('id');

        $listQuery = Transaction::query();
        
        if ($userSalesId) {
            $listQuery->where('sales_id', $userSalesId);
        }

        $listQuery->where('status', 1);
        
        $list = $listQuery->get();
        
        return view('transaction.index', compact('list'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $salesQuery = DB::table('sales');
        $defaultSalesID = null;

        if (Auth::user()->level == 2) {
            $userSalesId = DB::table('sales')->where('email', Auth::user()->email)->value('id');
            $salesQuery->where('id', $userSalesId);
            $defaultSalesID = $userSalesId;
        }
        
        $sales = $salesQuery->pluck('name', 'id');
        $barang = DB::table('barangs')->pluck('name','id');
        $outlet = DB::table('outlets')->pluck('name','id');
        
        return view('transaction.create', compact('barang', 'sales', 'outlet', 'defaultSalesID'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'sales_id.required' => 'ID Barang wajib diisi',
            'sales_name.required' => 'Nama sales wajib diisi',
            'barang_id.required' => 'ID Barang wajib diisi',
            'barang_name.required' => 'Nama barang wajib diisi',
            'outlet_id.required' => 'ID Barang wajib diisi',
            'outlet_name.required' => 'Nama outlet wajib diisi',
            'jumlah_stok.required' => 'Jumlah stok wajib diisi',
            'jumlah_stok.numeric' => 'Jumlah stok wajib diisi dengan angka',
            'jumlah_stok.min' => 'Lengkapi jumlah stok minimal 1',
            'jumlah_display.required' => 'Jumlah display wajib diisi',
            'jumlah_display.numeric' => 'Jumlah display wajib diisi dengan angka',
            'visit_datetime.required' => 'Tanggal visit wajib diisi',
            'visit_datetime.date' => 'Lengkapi tanggal dengan format yang sesuai',
            ];
        $validator = Validator::make($request->all(), [
            'sales_name'=> 'required',
            'barang_name'=> 'required',
            'outlet_name'=> 'required',
            'sales_id'=> 'required',
            'barang_id'=> 'required',
            'outlet_id'=> 'required',
            'jumlah_stok'=> 'required|numeric|min:1',
            'jumlah_display'=> 'required|numeric',
            'visit_datetime'=> 'required|date',
    ], $messages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::back()->withErrors($messages)->withInput($request->all());
        }
        
        $transaction = new Transaction();
        $transaction->sales_id= $request->input('sales_id');
        $transaction->sales_name= $request->input('sales_name');
        $transaction->barang_id= $request->input('barang_id');
        $transaction->barang_name= $request->input('barang_name');
        $transaction->outlet_id= $request->input('outlet_id');
        $transaction->outlet_name= $request->input('outlet_name');
        $transaction->jumlah_stok= $request->input('jumlah_stok');
        $transaction->jumlah_display= $request->input('jumlah_display');
        $transaction->visit_datetime= $request->input('visit_datetime');
        $transaction->created_by = Auth::user()->email;
        $transaction->save();

        return \redirect('transaction')->with('success', 'Ubah data berhasil');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $detail = Transaction::find($id);

        return view('transaction.detail', compact('detail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $detail = Transaction::find($id);
        $sales = DB::table('sales')->pluck('name','id');
        $barang = DB::table('barangs')->pluck('name','id');
        $outlet = DB::table('outlets')->pluck('name','id');
        return view('transaction.edit', compact('detail','sales','barang','outlet'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'sales_id.required' => 'ID Barang wajib diisi',
            'sales_name.required' => 'Nama sales wajib diisi',
            'barang_id.required' => 'ID Barang wajib diisi',
            'barang_name.required' => 'Nama barang wajib diisi',
            'outlet_id.required' => 'ID Barang wajib diisi',
            'outlet_name.required' => 'Nama outlet wajib diisi',
            'jumlah_stok.required' => 'Jumlah stok wajib diisi',
            'jumlah_stok.numeric' => 'Jumlah stok wajib diisi dengan angka',
            'jumlah_stok.min' => 'Lengkapi jumlah stok minimal 0',
            'jumlah_display.required' => 'Jumlah display wajib diisi',
            'jumlah_display.numeric' => 'Jumlah display wajib diisi dengan angka',
            'jumlah_display.min' => 'Lengkapi jumlah display minimal 0',
            'visit_datetime.required' => 'Tanggal visit wajib diisi',
            'visit_datetime.date' => 'Lengkapi tanggal dengan format yang sesuai',
            ];
        $validator = Validator::make($request->all(), [
            'sales_name'=> 'required',
            'barang_name'=> 'required',
            'outlet_name'=> 'required',
            'sales_id'=> 'required',
            'barang_id'=> 'required',
            'outlet_id'=> 'required',
            'jumlah_stok'=> 'required|numeric|min:0',
            'jumlah_display'=> 'required|numeric|min:0',
            'visit_datetime'=> 'required|date',
        ], $messages);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::back()->withErrors($messages)->withInput($request->all());
        }
        $transaction = Transaction::find($id);
        $transaction->sales_id= $request->input('sales_id');
        $transaction->sales_name= $request->input('sales_name');
        $transaction->barang_id= $request->input('barang_id');
        $transaction->barang_name= $request->input('barang_name');
        $transaction->outlet_id= $request->input('outlet_id');
        $transaction->outlet_name= $request->input('outlet_name');
        $transaction->jumlah_stok= $request->input('jumlah_stok');
        $transaction->jumlah_display= $request->input('jumlah_display');
        $transaction->visit_datetime= $request->input('visit_datetime');
        $transaction->save();

        return \redirect('transaction')->with('success', 'Ubah data berhasil');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $transaction = Transaction::find($id);
        $transaction->status = 9;
        $transaction->save();

        return \redirect('transaction')->with('success', 'Delete data berhasil');
    }
}
