<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Outlet extends Model
{
    protected $fillable = [
        'id',
        'name',
    ];
    
    const UPDATED_AT = null;
    protected $table = 'outlets';
}
