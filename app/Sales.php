<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sales extends Model
{
    protected $fillable = [
        'id',
        'name',
        'email',
        'password'
    ];

    const UPDATED_AT = null;
    protected $table = 'sales';
}
