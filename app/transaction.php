<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class transaction extends Model
{
    protected $fillable = [
        'id',
        'sales_id',
        'sales_name',
        'barang_id',
        'barang_name',
        'outlet_id',
        'outlet_name',
        'jumlah_stok',
        'jumlah_display',
        'visit_datetime'

    ];

    const UPDATED_AT = null;
    protected $table = 'transactions';
    protected $primaryKey='id';
    public $incrementing = false;
}
