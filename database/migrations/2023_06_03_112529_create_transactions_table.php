<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id')->autoIncrement();
            $table->unsignedInteger('sales_id');
            $table->string('sales_name', 100);
            $table->unsignedInteger('barang_id');
            $table->string('barang_name', 100);
            $table->unsignedInteger('outlet_id');
            $table->string('outlet_name', 100);
            $table->integer('jumlah_stok')->nullable();
            $table->integer('jumlah_display')->nullable();
            $table->timestamp('visit_datetime')->useCurrent();
            $table->unsignedInteger('status');
            $table->string('created_by', 25);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
