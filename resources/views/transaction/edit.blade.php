@extends('layout')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-tools">
                            <a href="{{ url('/transaction') }}" class="btn btn-success btn-sm pull-right"><i
                                    class="fa fa-pencil"></i> Kembali</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{url('transaction/'.$detail->id)}}">
                            @csrf
                            @method('PUT')
                            <h1>Form Update</h1>
                            <div class="form-group">
                                <label for="name" class="required">Sales</label>
                                @error('sales_id')
                                <div class="error">{{ $message }}</div>
                                @enderror
                                @error('sales_name')
                                <div class="error">{{ $message }}</div>
                                @enderror
                                {{ Form::select('sales_id',$sales, $defaultSalesID,['class' => 'form-control','placeholder'=> '-- Pilih --', 'id' => 'sales_select', 'disabled' => $defaultSalesID ? 'disabled' : null]) }}
                                <input type="hidden" name="sales_name" id="sales_name" value="{{ isset($defaultSalesID) ? $sales[$defaultSalesID] : '' }}">

                                <label for="name" class="required">Barang</label>
                                @error('barang_id')
                                <div class="error">{{ $message }}</div>
                                @enderror
                                @error('barang_name')
                                <div class="error">{{ $message }}</div>
                                @enderror
                                {{ Form::select('barang_id',$barang,  $detail['barang_id'],['class' => 'form-control','placeholder'=> '-- Pilih --', 'id' => 'barang_select']) }}
                                <input type="hidden" name="barang_name" id="barang_name" value={{$detail['barang_name']}}>

                                <label for="name" class="required">Outlet</label>
                                @error('outlet_id')
                                <div class="error">{{ $message }}</div>
                                @enderror
                                @error('outlet_name')
                                <div class="error">{{ $message }}</div>
                                @enderror
                                {{ Form::select('outlet_id',$outlet,  $detail['outlet_id'],['class' => 'form-control','placeholder'=> '-- Pilih --', 'id' => 'outlet_select']) }}
                                <input type="hidden" name="outlet_name" id="outlet_name" value={{$detail['outlet_name']}}>

                                <label for="stok" class="required">Jumlah Stok</label>
                                @error('jumlah_stok')
                                <div class="error">{{ $message }}</div>
                                @enderror
                                <input type="number" class="form-control" placeholder="Masukkan Jumlah Stok" name="jumlah_stok"
                                       id="jumlah_stok" value={{$detail['jumlah_stok']}}>

                                <label for="stok" class="required">Jumlah Display</label>
                                @error('jumlah_display')
                                <div class="error">{{ $message }}</div>
                                @enderror
                                <input type="number" class="form-control" placeholder="Masukkan Jumlah Display" name="jumlah_display"
                                       id="jumlah_display"  value={{$detail['jumlah_display']}}>

                                <label for="tanggal" class="required">Visit Datetime</label>
                                @error('visit_datetime')
                                @if($errors->has('visit_datetime'))
                                    <span class="help-block">
                                <strong>{{ $errors->first('visit_datetime') }}</strong>
                                </span>
                                @endif
                                <div class="error">{{ $message }}</div>
                                @enderror
                                <input type="datetime-local" class="form-control" placeholder="Pilih Tanggal" name="visit_datetime"  value={{$detail['visit_datetime']}}>

                                </div>
                            <button type="submit" class="form-control btn btn-primary">Submit</button>
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $(document).ready(function() {
            // Listen for the change event of the select element
            $('#sales_select').change(function() {
                // Get the selected option
                var selectedOption = $(this).children('option:selected');
                
                // Get the sales name from the selected option
                var salesName = selectedOption.text();
                
                // Set the value of the hidden input field
                $('#sales_name').val(salesName);
            });

            $('#barang_select').change(function() {
                // Get the selected option
                var selectedOption = $(this).children('option:selected');
                
                // Get the sales name from the selected option
                var barangName = selectedOption.text();
                
                // Set the value of the hidden input field
                $('#barang_name').val(barangName);
            });

            $('#outlet_select').change(function() {
                // Get the selected option
                var selectedOption = $(this).children('option:selected');
                
                // Get the sales name from the selected option
                var outletName = selectedOption.text();
                
                // Set the value of the hidden input field
                $('#outlet_name').val(outletName);
            });
        });
    </script>
@endsection
@section('custom-js-css')
    <script>
        function validasi(data) {
            if (data < 0) {
                alert('Data input tidak boleh kosong');
            }
        }

        jQuery(function ($) {
        });
    </script>
@endsection

