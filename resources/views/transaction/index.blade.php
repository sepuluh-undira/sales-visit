@extends('layout')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1>Transaction</h1>
                <div class="card">
                    <div class="card-header">
                        <div class="btn-add-new">
                            <a href="{{url('transaction/create')}}">
                                <button type="button" class="btn btn-dark">Add New</button>
                            </a>
                        </div>
                        <div class="card-tools">
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Sales</th>
                                <th scope="col">Barang</th>
                                <th scope="col">Outlet</th>
                                <th scope="col">Jumlah Stok</th>
                                <th scope="col">Jumlah Display</th>
                                <th scope="col">Visit</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i = 1
                            ?>
                            @foreach($list as $key => $value)
                                <tr>
                                    <th scope="row">{{ $i++ }}</th>
                                    <td>{{ $value->sales_name }}</td>
                                    <td>{{ $value->barang_name }}</td>
                                    <td>{{ $value->outlet_name }}</td>
                                    <td>{{ $value->jumlah_stok }}</td>
                                    <td>{{ $value->jumlah_display }}</td>
                                    <td>{{ $value->visit_datetime }}</td>
                                    <td>
                                        <a href="{{url('transaction/'.$value->id) }}">
                                            <button type="button" class="btn btn-dark">View
                                            </button>
                                        </a>
                                        <a href="{{url('transaction/'.$value->id) .'/edit'}}">
                                            <button type="button" class="btn btn-dark">Edit
                                            </button>
                                        </a>
                                        <form method="POST" action={{url('transaction/'.$value->id)}}>
                                            @csrf
                                            @method('DELETE')
                                            <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-dark">Delete
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>
    </div>
@endsection

