<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth', 'checkUserLevel'])->group(function (){
   Route::get('/',function() {
        return view('home');
   });

   Route::resource('barang', BarangController::class);
   Route::resource('outlet', OutletController::class);
   Route::resource('sales', SalesController::class);
});
Route::group(['middleware'=>'auth'], function(){
  
   Route::resource('transaction', TransactionController::class);
});

Route::post('register', 'AuthController@register');
Route::post('login', 'AuthController@login');
Route::post('logout', 'AuthController@logout')->middleware('auth.jwt');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
